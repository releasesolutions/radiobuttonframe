package br.com.releasesolutions;

// Figura 12.19: RadioButtonFrame.java
// Criando bot�es de op��o utilizando ButtonGroup e JRadioButton

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class RadioButtonFrame extends JFrame {

    private final JTextField textField; // utilizado para exibir altera��es de fonte
    private final Font plainFont; // fonte para texto simples
    private final Font boldFont; // fonte para texto em negrito
    private final Font italicFont; // fonte para texto em it�lico
    private final Font boldAndItalicFont; // fonte para texto em negrito e it�lico
    private final JRadioButton plainJRadioButton; // seleciona texto simples
    private final JRadioButton boldRadionButton; // seleciona texto em negrito
    private final JRadioButton italicRadioButton; // seleciona texto em it�lico
    private final JRadioButton boldAndItalicRadioButton; // netrito e it�lico
    private final ButtonGroup radioGroup; // cont�m bot�es de op��o

    public RadioButtonFrame() {
        super("RadioButton Test");
        setLayout(new FlowLayout());

        textField = new JTextField("Watch the font style change", 25);
        add(textField); // adiciona textField ao JFrame

        // cria bot�es de op��o
        plainJRadioButton = new JRadioButton("Plain", true);
        boldRadionButton = new JRadioButton("Bold", false);
        italicRadioButton = new JRadioButton("Italic", false);
        boldAndItalicRadioButton = new JRadioButton("Bold/Italic", false);

        add(plainJRadioButton); // adiciona bot�o no estilo simples ao JFrame
        add(boldRadionButton); // adiciona bot�o de negrito ao JFrame
        add(italicRadioButton); // adiciona bot�o de it�lico ao JFrame
        add(boldAndItalicRadioButton); // adiciona bot�o de negrito e it�lico

        // cria relacionamento l�gico entre JRadioButtons
        radioGroup = new ButtonGroup(); // cria ButtonGroup
        radioGroup.add(plainJRadioButton); // adiciona texto simples ao grupo
        radioGroup.add(boldRadionButton); // adiciona negrito ao grupo
        radioGroup.add(italicRadioButton); // adiciona it�lico ao grupo
        radioGroup.add(boldAndItalicRadioButton); // adiciona negrito e it�lico

        // cria fonte objetos
        plainFont = new Font("Serif", Font.PLAIN, 14);
        boldFont = new Font("Serif", Font.BOLD, 14);
        italicFont = new Font("Serif", Font.ITALIC, 14);
        boldAndItalicFont = new Font("Serif", Font.BOLD + Font.ITALIC, 14);
        textField.setFont(plainFont);

        // registra os eventos para JRadioButtons
        plainJRadioButton.addItemListener(new RadioButtonHandler(plainFont));
        boldRadionButton.addItemListener(new RadioButtonHandler(boldFont));
        italicRadioButton.addItemListener(new RadioButtonHandler(italicFont));
        boldAndItalicRadioButton.addItemListener(new RadioButtonHandler(boldAndItalicFont));
    }

    // classe interna private para tratar eventos de bot�o de op��o
    private class RadioButtonHandler implements ItemListener {

        private Font fonte; // fonte associada com esse listener

        public RadioButtonHandler(Font fonte) {
            this.fonte = fonte;
        }

        // trata eventos de bot�o de op��o
        @Override
        public void itemStateChanged(ItemEvent event) {
            textField.setFont(fonte);
        }
    }
} // fim da classe RadionButtonFrame
